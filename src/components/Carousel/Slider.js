import React from 'react'
import PropTypes from 'prop-types'
import Slick from 'react-slick'
import YouTube from 'react-youtube'

const Slider = ({ slides, setSlider, changeHandler, setYTPlayers, currentIndex }) => {
  // render slides method
  const renderItem = (slide, index) => {
    switch (slide.asset_type.toLowerCase()) {
      case 'website':
        return (
          <iframe
            className='carousel__iframe fade'
            title={slide.title}
            src={slide.file}
          />
        )
      case 'image':
        return (
          <div
            className='carousel__image'
            style={{
              background: `url(${slide.file}) no-repeat center center`,
              backgroundSize: 'cover'
            }}
          />
        )
      case 'widget_youtube':
        return (
          <YouTube
            className='carousel__iframe'
            videoId={slide.embed_code}
            opts={{
              playerVars: {
                loop: 1,
                playlist: slide.embed_code,
                autoplay: index === 0 ? 1 : 0,
                mute: 1
              }
            }}
            onReady={e => setYTPlayers({ index, yt: e.target })}
          />

        )
      case 'video':
        return (
          <video
            id={`carousel-video-${index}`}
            className='carousel__video'
            controls
            loop
          >
            {slide.webm_url && <source src={slide.webm_url} type='video/webm' />}
            {slide.file && <source src={slide.iphone_url} type='video/mp4' />}
            {slide.ogv_url && <source src={slide.ogv_url} type='video/ogg' />}
          </video>
        )
      default:
        return false
    }
  }

  return (
    <Slick
      infinite={false}
      dots={false}
      arrows={false}
      slide={false}
      swipe={false}
      slideToShow={1}
      slideToScroll={1}
      className='carousel__carousel'
      ref={c => setSlider(c)}
      beforeChange={i => changeHandler(i, 'before')}
      afterChange={i => changeHandler(i, 'after')}
      useTransition={false}
      useCSS={false}
      fade
    >
      {slides.map((slide, index) => (
        <div
          key={index}
          className='carousel__item'
        >
          <div
            style={{
              width: '100%',
              height: '100%',
              opacity: index === currentIndex ? 1 : 0,
              transition: '1250ms',
              animation: index === currentIndex ? `${slide.transitions} 1250ms` : ''
            }}
          >
            {renderItem(slide, index)}
          </div>
        </div>
      ))}
    </Slick>
  )
}

Slider.propTypes = {
  slides: PropTypes.array.isRequired,
  setSlider: PropTypes.func.isRequired,
  changeHandler: PropTypes.func.isRequired,
  setYTPlayers: PropTypes.func.isRequired,
  currentIndex: PropTypes.number
}

export default Slider
