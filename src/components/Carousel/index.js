import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import Slider from './Slider'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import './animations.css'
import './styles.sass'

const Carousel = ({
  slides = [],
  onSlideChange = f => f,
  duration: customDuration = null
}) => {
  // variables for control slider
  // slide - current slide
  // index - current slide index
  // slider - slick slider object
  // ytPlayers - array of youtube pleyers
  const [slide, setSlide] = useState(null)
  const [index, setIndex] = useState(0)
  const [slider, setSlider] = useState(null)
  const [ytPlayers, setYTPlayers] = useState([])

  // when slides array passed
  // first array element sets to current slide
  useEffect(() => {
    if (slides.length) {
      setSlide(slides[0])
    }
  }, [slides])

  // timeout for changing current slide
  // duration - custom duration or each slide duration converted to seconds
  // time - if slide has transition added 1.25 sec to duration for effect
  useEffect(() => {
    if (slides.length && slide) {
      const duration = (customDuration || slide.duration) * 1000
      const time = slide.transitions ? duration + 1250 : duration
      const nextSlideIndex = index !== slides.length - 1 ? index + 1 : 0
      const timeout = setTimeout(() => {
        slider.slickGoTo(nextSlideIndex)
      }, time)

      return () => {
        clearTimeout(timeout)
      }
    }
    // eslint-disable-next-line
  }, [slide, slider])

  // this handler calls automatically by slick library before and after slide changes
  // i - next slide index, t - event 'after' or 'before'
  const changeHandler = (i, t) => {
    if (slides.length) {
      setIndex(i)
      const slide = slides[i]

      if (t === 'after') {
        // updated current slide
        setSlide(slide)
        // called callback passes to component
        onSlideChange({ index: i })
      }

      const type = slide.asset_type.toLowerCase()

      // after slide change checking slide type and play video or youtube player
      switch (type) {
        case 'video':
          handleVideoPlayer(t, i)
          break
        case 'widget_youtube':
          handleYoutubePlayer(t, i)
          break
        default:
          break
      }
    }
  }

  const handleVideoPlayer = (t, i) => {
    const video = document.querySelector(`#carousel-video-${i}`)
    if (video) {
      if (t === 'after') {
        setTimeout(() => {
          video.play()
        }, 500)
      } else if (t === 'before') {
        video.currentTime = 0
        video.pause()
      }
    }
  }

  // youtube player handler
  // looking for needed player by id
  const handleYoutubePlayer = (t, i) => {
    const yt = ytPlayers.find(item => item.index === i)

    if (yt) {
      if (t === 'after') {
        yt.yt.playVideo()
      } else {
        yt.yt.seekTo(0)
        yt.yt.pauseVideo()
      }
    }
  }

  // when youtube player with video loaded this method called from Slider.js component
  // and player pushes to players array
  const setYTPlayersHandler = item => {
    const playersCopy = [...ytPlayers]
    playersCopy.push(item)
    setYTPlayers(playersCopy)

    if (item.index === index) {
      item.yt.playVideo()
    }
  }

  return (
    <div
      className='carousel__wrapper'
    >
      <Slider
        slides={slides}
        setSlider={setSlider}
        changeHandler={changeHandler}
        setYTPlayers={setYTPlayersHandler}
        currentIndex={index}
      />
    </div>
  )
}

Carousel.propTypes = {
  slides: PropTypes.array.isRequired,
  onSlideChange: PropTypes.func,
  duration: PropTypes.number
}

export default Carousel
