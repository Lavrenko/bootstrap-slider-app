import React, { useEffect, useState } from 'react'

import Carousel from './components/Carousel'

import './App.sass'

function App () {
  const [data, setData] = useState([])
  useEffect(() => {
    const getData = async () => {
      const response = await window.fetch('https://stage.signstream.net/api/v1/channels/stage-86')
      const data = await response.json()
      const mapped = data.filter(item => !!item.duration && !!item.asset_type)
      setData(mapped)
    }
    getData()
  }, [])

  const onSlideChange = e => {
    console.log(e)
  }

  return (
    <Carousel
      slides={data}
      onSlideChange={onSlideChange}
      duration={15}
    />
  )
}

export default App
